import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.BorderLayout;
import javax.swing.SpringLayout;
import javax.swing.JPanel;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JCheckBox;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.ActionEvent;

public class Ex_Spring {

	private JFrame frame;
	private JTextField txt_id;
	private JTextField txt_pw;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ex_Spring window = new Ex_Spring();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Ex_Spring() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 809, 542);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		SpringLayout springLayout = new SpringLayout();
		frame.getContentPane().setLayout(springLayout);
		
		JPanel panel = new JPanel();
		springLayout.putConstraint(SpringLayout.NORTH, panel, 10, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, panel, 10, SpringLayout.WEST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, panel, 337, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, panel, 263, SpringLayout.WEST, frame.getContentPane());
		panel.setBackground(Color.MAGENTA);
		panel.setForeground(Color.MAGENTA);
		frame.getContentPane().add(panel);
		SpringLayout sl_panel = new SpringLayout();
		panel.setLayout(sl_panel);
		
		JLabel lbl_ID = new JLabel("\uC544\uC774\uB514\uB97C \uC785\uB825\uD558\uC138\uC694");
		sl_panel.putConstraint(SpringLayout.NORTH, lbl_ID, 78, SpringLayout.NORTH, panel);
		sl_panel.putConstraint(SpringLayout.WEST, lbl_ID, 36, SpringLayout.WEST, panel);
		lbl_ID.setFont(new Font("����", Font.PLAIN, 18));
		lbl_ID.setHorizontalAlignment(SwingConstants.CENTER);
		panel.add(lbl_ID);
		
		txt_id = new JTextField();
		sl_panel.putConstraint(SpringLayout.NORTH, txt_id, 27, SpringLayout.SOUTH, lbl_ID);
		sl_panel.putConstraint(SpringLayout.WEST, txt_id, 0, SpringLayout.WEST, lbl_ID);
		sl_panel.putConstraint(SpringLayout.EAST, txt_id, 0, SpringLayout.EAST, lbl_ID);
		panel.add(txt_id);
		txt_id.setColumns(10);
		
		JPanel panel_1 = new JPanel();
		springLayout.putConstraint(SpringLayout.NORTH, panel_1, 10, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, panel_1, -134, SpringLayout.EAST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, panel_1, -220, SpringLayout.SOUTH, panel);
		springLayout.putConstraint(SpringLayout.EAST, panel_1, -10, SpringLayout.EAST, frame.getContentPane());
		frame.getContentPane().add(panel_1);
		
		JPanel panel_2 = new JPanel();
		springLayout.putConstraint(SpringLayout.WEST, panel_2, 47, SpringLayout.EAST, panel);
		springLayout.putConstraint(SpringLayout.EAST, panel_2, -171, SpringLayout.WEST, panel_1);
		
		JLabel lblNewLabel = new JLabel("\uBE44\uBC00\uBC88\uD638\uB97C \uC785\uB825\uD558\uC138\uC694");
		sl_panel.putConstraint(SpringLayout.NORTH, lblNewLabel, 47, SpringLayout.SOUTH, txt_id);
		sl_panel.putConstraint(SpringLayout.WEST, lblNewLabel, 21, SpringLayout.WEST, panel);
		sl_panel.putConstraint(SpringLayout.SOUTH, lblNewLabel, 68, SpringLayout.SOUTH, txt_id);
		sl_panel.putConstraint(SpringLayout.EAST, lblNewLabel, 23, SpringLayout.EAST, lbl_ID);
		lblNewLabel.setFont(new Font("����", Font.PLAIN, 18));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		panel.add(lblNewLabel);
		
		txt_pw = new JTextField();
		sl_panel.putConstraint(SpringLayout.NORTH, txt_pw, 40, SpringLayout.SOUTH, lblNewLabel);
		sl_panel.putConstraint(SpringLayout.WEST, txt_pw, 62, SpringLayout.WEST, panel);
		panel.add(txt_pw);
		txt_pw.setColumns(10);
		
		JLabel lbl_c_id = new JLabel("New label");
		lbl_c_id.setHorizontalAlignment(SwingConstants.CENTER);
		sl_panel.putConstraint(SpringLayout.NORTH, lbl_c_id, 10, SpringLayout.NORTH, panel);
		sl_panel.putConstraint(SpringLayout.WEST, lbl_c_id, 93, SpringLayout.WEST, panel);
		panel.add(lbl_c_id);
		
		JLabel lbl_c_pw = new JLabel("New label");
		lbl_c_pw.setHorizontalAlignment(SwingConstants.CENTER);
		sl_panel.putConstraint(SpringLayout.NORTH, lbl_c_pw, 17, SpringLayout.SOUTH, lbl_c_id);
		sl_panel.putConstraint(SpringLayout.EAST, lbl_c_pw, 0, SpringLayout.EAST, lbl_c_id);
		panel.add(lbl_c_pw);
		panel_2.setBackground(Color.YELLOW);
		springLayout.putConstraint(SpringLayout.NORTH, panel_2, 36, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, panel_2, 304, SpringLayout.NORTH, frame.getContentPane());
		frame.getContentPane().add(panel_2);
		SpringLayout sl_panel_2 = new SpringLayout();
		panel_2.setLayout(sl_panel_2);
		
		JCheckBox cbx_male = new JCheckBox("\uB0A8\uC790");
		cbx_male.setHorizontalAlignment(SwingConstants.CENTER);
		panel_2.add(cbx_male);
		
		cbx_male.addItemListener(new ItemListener() {
			
			@Override
			public void itemStateChanged(ItemEvent e) {
				// TODO Auto-generated method stub
				System.out.println(e.getStateChange());
				
			}
		});
		
		JCheckBox cbx_female = new JCheckBox("\uC5EC\uC790");
		sl_panel_2.putConstraint(SpringLayout.WEST, cbx_male, 0, SpringLayout.WEST, cbx_female);
		sl_panel_2.putConstraint(SpringLayout.SOUTH, cbx_male, -27, SpringLayout.NORTH, cbx_female);
		sl_panel_2.putConstraint(SpringLayout.NORTH, cbx_female, 121, SpringLayout.NORTH, panel_2);
		sl_panel_2.putConstraint(SpringLayout.WEST, cbx_female, 55, SpringLayout.WEST, panel_2);
		cbx_female.setBackground(Color.MAGENTA);
		cbx_female.setForeground(Color.GREEN);
		cbx_female.setHorizontalAlignment(SwingConstants.CENTER);
		panel_2.add(cbx_female);
		
		JLabel lbl_sex = new JLabel("\uB2F9\uC2E0\uC758 \uC131\uBCC4\uC740?");
		lbl_sex.setHorizontalAlignment(SwingConstants.CENTER);
		sl_panel_2.putConstraint(SpringLayout.NORTH, lbl_sex, 24, SpringLayout.NORTH, panel_2);
		sl_panel_2.putConstraint(SpringLayout.WEST, lbl_sex, 0, SpringLayout.WEST, cbx_male);
		panel_2.add(lbl_sex);
		
		JButton btnNewButton = new JButton("\uD655\uC778");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println(txt_id.getText() + " : " + txt_pw.getText());
				String changeID = txt_id.getText();
				String changePW = txt_pw.getText();
				
				lbl_c_id.setText(changeID);
				lbl_c_pw.setText(changePW);
			}
		});
		btnNewButton.setBackground(Color.MAGENTA);
		btnNewButton.setForeground(Color.GREEN);
		btnNewButton.setFont(new Font("����", Font.PLAIN, 15));
		springLayout.putConstraint(SpringLayout.WEST, btnNewButton, 238, SpringLayout.WEST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, btnNewButton, -28, SpringLayout.SOUTH, frame.getContentPane());
		frame.getContentPane().add(btnNewButton);
	}
}
