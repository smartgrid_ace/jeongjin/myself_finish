package Member;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.SpringLayout;
import javax.swing.JPanel;
import javax.swing.JTextField;
import java.awt.CardLayout;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class MemberGUI {

	private JFrame frame;
	private JTextField txt_id;
	private JTextField txt_pw;
	MemberDAO dao = new MemberDAO();

	Member_dial dialog;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MemberGUI window = new MemberGUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MemberGUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.DARK_GRAY);
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		SpringLayout springLayout = new SpringLayout();
		frame.getContentPane().setLayout(springLayout);
		
		JPanel panel_1 = new JPanel();
		springLayout.putConstraint(SpringLayout.NORTH, panel_1, 10, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, panel_1, 10, SpringLayout.WEST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, panel_1, 47, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, panel_1, 413, SpringLayout.WEST, frame.getContentPane());
		panel_1.setBackground(Color.DARK_GRAY);
		frame.getContentPane().add(panel_1);
		panel_1.setLayout(new CardLayout(0, 0));
		
		JLabel lbl_login = new JLabel("\uB85C\uADF8\uC778");
		lbl_login.setFont(new Font("����", Font.PLAIN, 30));
		lbl_login.setForeground(Color.WHITE);
		lbl_login.setHorizontalAlignment(SwingConstants.CENTER);
		panel_1.add(lbl_login, "name_32304795601386");
		
		JPanel panel = new JPanel();
		springLayout.putConstraint(SpringLayout.NORTH, panel, -213, SpringLayout.SOUTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, panel, 413, SpringLayout.WEST, frame.getContentPane());
		panel.setBackground(Color.DARK_GRAY);
		springLayout.putConstraint(SpringLayout.WEST, panel, 10, SpringLayout.WEST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, panel, -10, SpringLayout.SOUTH, frame.getContentPane());
		frame.getContentPane().add(panel);
		SpringLayout sl_panel = new SpringLayout();
		panel.setLayout(sl_panel);
		
		txt_id = new JTextField();
		sl_panel.putConstraint(SpringLayout.WEST, txt_id, -223, SpringLayout.EAST, panel);
		sl_panel.putConstraint(SpringLayout.SOUTH, txt_id, -129, SpringLayout.SOUTH, panel);
		sl_panel.putConstraint(SpringLayout.EAST, txt_id, -40, SpringLayout.EAST, panel);
		panel.add(txt_id);
		txt_id.setColumns(10);
		
		JLabel lbl_id = new JLabel("\uC544\uC774\uB514");
		sl_panel.putConstraint(SpringLayout.NORTH, txt_id, 0, SpringLayout.NORTH, lbl_id);
		lbl_id.setForeground(Color.WHITE);
		sl_panel.putConstraint(SpringLayout.NORTH, lbl_id, 50, SpringLayout.NORTH, panel);
		lbl_id.setFont(new Font("����", Font.PLAIN, 20));
		sl_panel.putConstraint(SpringLayout.WEST, lbl_id, 44, SpringLayout.WEST, panel);
		panel.add(lbl_id);
		
		txt_pw = new JTextField();
		sl_panel.putConstraint(SpringLayout.EAST, txt_pw, -40, SpringLayout.EAST, panel);
		panel.add(txt_pw);
		txt_pw.setColumns(10);
		
		JLabel lbl_pw = new JLabel("\uBE44\uBC00\uBC88\uD638");
		sl_panel.putConstraint(SpringLayout.NORTH, txt_pw, 0, SpringLayout.NORTH, lbl_pw);
		sl_panel.putConstraint(SpringLayout.WEST, txt_pw, 56, SpringLayout.EAST, lbl_pw);
		sl_panel.putConstraint(SpringLayout.SOUTH, txt_pw, 0, SpringLayout.SOUTH, lbl_pw);
		lbl_pw.setForeground(Color.WHITE);
		lbl_pw.setFont(new Font("����", Font.PLAIN, 20));
		sl_panel.putConstraint(SpringLayout.NORTH, lbl_pw, 31, SpringLayout.SOUTH, lbl_id);
		sl_panel.putConstraint(SpringLayout.WEST, lbl_pw, 0, SpringLayout.WEST, lbl_id);
		panel.add(lbl_pw);
		JButton btn_new = new JButton("\uD68C\uC6D0\uAC00\uC785");
		btn_new.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dialog = new Member_dial();
				dialog.setVisible(true);
			}
		});
		
	
		sl_panel.putConstraint(SpringLayout.EAST, btn_new, 0, SpringLayout.EAST, lbl_pw);
		panel.add(btn_new);
		
		JButton btn_login = new JButton("\uB85C\uADF8\uC778");
		btn_login.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String id = txt_id.getText();
				String pw = txt_pw.getText();
				boolean isRunning = dao.login(id, pw);
				if(isRunning) {
					lbl_login.setText(id+"�� �α����ϼ̽��ϴ�.");
					
				}else {
					System.out.println("�α��ο� �����ϼ̽��ϴ�");
				}
			
			}
		});
		sl_panel.putConstraint(SpringLayout.SOUTH, btn_login, -10, SpringLayout.SOUTH, panel);
		sl_panel.putConstraint(SpringLayout.EAST, btn_login, -167, SpringLayout.EAST, panel);
		sl_panel.putConstraint(SpringLayout.NORTH, btn_new, 0, SpringLayout.NORTH, btn_login);
		panel.add(btn_login);
		
		JButton btn_delete = new JButton("\uD68C\uC6D0\uD0C8\uD1F4");
		sl_panel.putConstraint(SpringLayout.NORTH, btn_delete, 0, SpringLayout.NORTH, btn_new);
		sl_panel.putConstraint(SpringLayout.WEST, btn_delete, 37, SpringLayout.EAST, btn_login);
		panel.add(btn_delete);
		
		
	}

}
