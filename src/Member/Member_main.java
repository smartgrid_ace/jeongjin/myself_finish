package Member;

import java.util.Scanner;

public class Member_main {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		MemberDAO dao = new MemberDAO();
		while(true) {
		System.out.println("[1] 회원가입[2] 로그인[3] 회원탈퇴[4] 종료");
		int choice = sc.nextInt();
		if(choice == 1) {
			System.out.print("아이디 :");
			String joinId = sc.next();
			System.out.print("비밀번호 : ");
			String joinPw = sc.next();
			System.out.print("나이 : ");
			int joinAge = sc.nextInt();
			System.out.print("전화번호 : ");
			String joinTel = sc.next();
			MemberDTO dto = new MemberDTO(joinId, joinPw, joinAge, joinTel);
			int num = dao.join(dto);
			
			if(num>0) {
				System.out.println(joinId+"님 회원가입을 환영합니다.");
			}else {
				System.out.println("회원가입이 실패 하였습니다");
			}
		}else if(choice == 2) {
			System.out.print("아이디를 입력하세요 : ");
			String loginId = sc.next();
			System.out.print("비밀번호를 입력하세요 : ");
			String loginPw = sc.next();
			
			if(dao.login(loginId,loginPw)) {
				System.out.println("로그인 성공");
			}else {
				System.out.println("로그인 실패");
			}
		}else if(choice == 3) {
			System.out.print("삭제할 아이디를 입력하세요 : ");
			String deleteId = sc.next();
			dao.delete(deleteId);
		}else if(choice == 4) {
			System.out.println("종료 합니다");
			break;
		}
		}
	}

}
