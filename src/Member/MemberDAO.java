package Member;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class MemberDAO {
	Connection conn = null;
	PreparedStatement pst = null;
	
	//동적로딩
	public MemberDAO() {
		try {
		String url = "jdbc:oracle:thin:@127.0.0.1:1521:xe";
		String dbId = "hr";
		String dbPw = "hr";
		Class.forName("oracle.jdbc.driver.OracleDriver");
		
		conn = DriverManager.getConnection(url, dbId, dbPw);
		if(conn != null) {
			System.out.println("연결 성공");
		}else {
			System.out.println("연결 실패");
		}
		
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
		//객체 생성시 바로 실행
	}

		public int join(MemberDTO dto) {
			try {
				String sql = "insert into mem values(?,?,?,?)";
				pst = conn.prepareStatement(sql);
				pst.setString(1, dto.getId());
				pst.setString(2, dto.getPw());
				pst.setInt(3, dto.getAge());
				pst.setString(4, dto.getTel());
				
				int cnt = pst.executeUpdate();
				return cnt;
//				if(cnt>0) {
//					System.out.println("회원가입 성공");
//				}else {
//					System.out.println("실패");
//				}
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
				return 0;
		
			
		}

		public boolean login(String id, String pw) {
			
			try {
				String sql = "select id,pw from mem where id=? and pw=?";
				pst = conn.prepareStatement(sql);
				pst.setString(1, id);
				pst.setString(2, pw);
				ResultSet rs = pst.executeQuery();
			
				if(rs.next()) {
					return true;
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return false;
			
		}

		public int delete(String id) {
			
			try {
				String sql = "delete from mem where id=? ";
				pst = conn.prepareStatement(sql);
				pst.setString(1, id);
				int cnt = pst.executeUpdate();
				
				return cnt;
//				if(cnt>0) {
//					System.out.println("삭제되었습니다");
//				}else {
//					System.out.println("삭제에 실패하였습니다");
//				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return 0;
		}
		
	
}
