package Member;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.SpringLayout;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;

public class Member_dial extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JLabel lbl_id;
	private JLabel lbl_pw;
	private JTextField txt_id;
	private JTextField txt_pw;
	private JTextField txt_age;
	private JLabel lbl_age;
	private JTextField txt_tel;
	private JLabel lbl_tel;
	MemberDAO dao = new MemberDAO();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			Member_dial dialog = new Member_dial();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public Member_dial() {
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		SpringLayout sl_contentPanel = new SpringLayout();
		contentPanel.setLayout(sl_contentPanel);
		{
			lbl_id = new JLabel("\uC544\uC774\uB514");
			lbl_id.setHorizontalAlignment(SwingConstants.CENTER);
			contentPanel.add(lbl_id);
		}
		{
			lbl_pw = new JLabel("\uD328\uC2A4\uC6CC\uB4DC");
			lbl_pw.setHorizontalAlignment(SwingConstants.CENTER);
			sl_contentPanel.putConstraint(SpringLayout.NORTH, lbl_pw, 37, SpringLayout.SOUTH, lbl_id);
			sl_contentPanel.putConstraint(SpringLayout.WEST, lbl_pw, 26, SpringLayout.WEST, contentPanel);
			sl_contentPanel.putConstraint(SpringLayout.EAST, lbl_id, 0, SpringLayout.EAST, lbl_pw);
			contentPanel.add(lbl_pw);
		}
		{
			lbl_age = new JLabel("\uB098\uC774");
			lbl_age.setHorizontalAlignment(SwingConstants.CENTER);
			sl_contentPanel.putConstraint(SpringLayout.WEST, lbl_age, 0, SpringLayout.WEST, lbl_pw);
			sl_contentPanel.putConstraint(SpringLayout.EAST, lbl_age, 48, SpringLayout.WEST, lbl_id);
			contentPanel.add(lbl_age);
		}
		{
			lbl_tel = new JLabel("\uC804\uD654\uBC88\uD638");
			lbl_tel.setHorizontalAlignment(SwingConstants.CENTER);
			sl_contentPanel.putConstraint(SpringLayout.SOUTH, lbl_age, -33, SpringLayout.NORTH, lbl_tel);
			sl_contentPanel.putConstraint(SpringLayout.EAST, lbl_tel, 0, SpringLayout.EAST, lbl_id);
			contentPanel.add(lbl_tel);
		}
		{
			txt_id = new JTextField();
			sl_contentPanel.putConstraint(SpringLayout.NORTH, txt_id, 27, SpringLayout.NORTH, contentPanel);
			sl_contentPanel.putConstraint(SpringLayout.WEST, txt_id, 47, SpringLayout.EAST, lbl_id);
			sl_contentPanel.putConstraint(SpringLayout.NORTH, lbl_id, 3, SpringLayout.NORTH, txt_id);
			contentPanel.add(txt_id);
			txt_id.setColumns(10);
		}
		{
			txt_pw = new JTextField();
			sl_contentPanel.putConstraint(SpringLayout.NORTH, txt_pw, 0, SpringLayout.NORTH, lbl_pw);
			sl_contentPanel.putConstraint(SpringLayout.EAST, txt_pw, 0, SpringLayout.EAST, txt_id);
			contentPanel.add(txt_pw);
			txt_pw.setColumns(10);
		}
		{
			txt_age = new JTextField();
			sl_contentPanel.putConstraint(SpringLayout.NORTH, lbl_age, 0, SpringLayout.NORTH, txt_age);
			sl_contentPanel.putConstraint(SpringLayout.NORTH, txt_age, 25, SpringLayout.SOUTH, txt_pw);
			sl_contentPanel.putConstraint(SpringLayout.WEST, txt_age, 0, SpringLayout.WEST, txt_id);
			contentPanel.add(txt_age);
			txt_age.setColumns(10);
		}
		{
			txt_tel = new JTextField();
			sl_contentPanel.putConstraint(SpringLayout.SOUTH, txt_tel, -16, SpringLayout.SOUTH, contentPanel);
			sl_contentPanel.putConstraint(SpringLayout.NORTH, lbl_tel, 3, SpringLayout.NORTH, txt_tel);
			sl_contentPanel.putConstraint(SpringLayout.EAST, txt_tel, 0, SpringLayout.EAST, txt_id);
			contentPanel.add(txt_tel);
			txt_tel.setColumns(10);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						String id = txt_id.getText();
						String pw = txt_pw.getText();
						String age = txt_age.getText();
						String tel = txt_tel.getText();
						int changeAge = Integer.parseInt(age);
						MemberDTO dto = new MemberDTO(id, pw, changeAge, tel);
						int cnt = dao.join(dto);
						if (cnt > 0) {
							System.out.println("����");
						} else {
							System.out.println("����");
						}
						setVisible(false);

					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}

}
